﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseCommand;

namespace ObjectSelectionCommand
{
    #region Object Selection
    class SelectionCommand : Command
    {
        public override void execute()
        {
            //Base class for all commands that pertain to selection of objects
            Debug.Log("Base class for all selection related commands");
        }

        public override void execute(BaseCommand.ObjectType target)
        {

        }

        static public GameObject selectedObject;
        static public bool objectSelected = false;
    }

    class SpawnCommand : SelectionCommand
    {
        public override void execute(BaseCommand.ObjectType target)
        {
            //Spawns our object and selects it
            Spawn(target);
        }

        public void Spawn(BaseCommand.ObjectType target)
        {
            objectSelected = true;
            selectedObject = MonoBehaviour.Instantiate(levelObjects[(int)target]);
            objectsInScene.Add(selectedObject);
            objectsThisSession.Add(selectedObject);
        }

        public void Undo()
        {
            //Will only remove objects added this session, will not undo anything from before
            if (objectsThisSession.Count >= 1)
            {
                //Gets game object
                GameObject obj = objectsThisSession[objectsThisSession.Count-1];

                //Adds to redo list
                objectsToRedo.Add(new ObjectInfo(obj.transform.position, obj.transform.rotation.eulerAngles, (ObjectType)int.Parse(obj.tag)));

                //removes from session list
                objectsThisSession.RemoveAt(objectsThisSession.Count-1);
                //Removes from main list
                objectsInScene.RemoveAt(objectsInScene.Count-1);

                //Destroys instance
                MonoBehaviour.Destroy(obj);
            }
            else
            {
                Debug.Log("Nothing Left to undo!");
            }
        }

        public void Redo()
        {
            if (objectsToRedo.Count >= 1)
            {
                //Gets object info
                ObjectInfo info = objectsToRedo[objectsToRedo.Count-1];

                //Adds to session list
                Quaternion rot = Quaternion.Euler(info.rotation.x, info.rotation.y, info.rotation.z);

                objectsThisSession.Add(MonoBehaviour.Instantiate(levelObjects[(int)info.type], info.position, rot));
                //Adds to main list
                objectsInScene.Add(objectsThisSession[objectsThisSession.Count-1]);

                //removes from redo list
                objectsToRedo.RemoveAt(objectsToRedo.Count-1);
            }
            else
            {
                Debug.Log("Nothing Left to redo!");
            }
        }

        public List<GameObject> objectsThisSession = new List<GameObject>();
        public List<ObjectInfo> objectsToRedo = new List<ObjectInfo>();
    }

    class DragCommand : SelectionCommand
    {
        public override void execute()
        {
            //Drags the selected object from point A to point B
            Drag();
        }

        public void Drag()
        {
            //Gets Mouse position
            Vector3 mousePoint = Input.mousePosition;
            //Counteracts the camera z-position
            mousePoint.z = 10.0f;
            //Converts mouse position to world position
            Vector3 p = Camera.main.ScreenToWorldPoint(mousePoint);
            //Set transform position to p, as well as allows you to scroll to move up and down on z-axis
            selectedObject.transform.position = new Vector3(p.x, p.y, selectedObject.transform.position.z + Input.mouseScrollDelta.y);
            //Stops dragging if mouse is let go of
            if (Input.GetMouseButtonUp(0))
            {
                objectSelected = false;
                //The object is still stored as a reference after you let go so you can click and drag it again after
            }
        }
    }

    class SelectCommand : SelectionCommand
    {
        public override void execute()
        {
            //Selects the object

        }


    }
    #endregion

}