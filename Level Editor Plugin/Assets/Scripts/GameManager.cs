﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseCommand;
using ObjectSelectionCommand;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<GameObject> levelObjects;
    public InputField inputField;
    public GameObject playerPrefab;

    public void LoadButton()
    {
        GameManagerSingleton.GetInstance().LoadLevel(inputField.text);
        GameManagerSingleton.GetInstance().SpawnPlayer(playerPrefab);
    }

    public void EditButton()
    {
        GameManagerSingleton.GetInstance().SwitchToEdit();
    }

    private void Start()
    {
        SpawnCommand.levelObjects = levelObjects;
        
    }
}

class SingletonClass
{
    //Private constructor
    protected SingletonClass()
    {
        //This is so we can't call 
        //"new SingletonClass()"
        //anywhere other than within our member functions
    }

    //Gets instance
    public static SingletonClass GetInstance()
    {
        //if instance doesn't exist, one will be created
        if (singletonInstance == null)
        {
            singletonInstance = new SingletonClass();
        }

        //returns our instance
        return singletonInstance;
    }
    
    //Stores our instance
    protected static SingletonClass singletonInstance = null;
}

class GameManagerSingleton : SingletonClass
{
    public void LoadLevel(string levelName)
    {
        GameObject[] cars = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject go in cars)
            GameObject.Destroy(go);


        loadCommand = new FileIOCommand.LoadCommand();

        string temp = levelName + ".json";
        FileIOCommand.FileCommand.fileName = temp.Replace(" ", string.Empty);
        loadCommand.Load();
    }

    public void SwitchToEdit()
    {
        SceneManager.LoadScene("EditorScene");
    }

    public void SpawnPlayer(GameObject prefab)
    {
        GameObject[] spawners = GameObject.FindGameObjectsWithTag("7");

        GameObject go = GameObject.Instantiate(prefab);
        go.transform.position = spawners[0].transform.position;
        foreach (GameObject g in spawners)
            GameObject.Destroy(g);
    }

    //Gets instance
    public new static GameManagerSingleton GetInstance()
    {
        //if instance doesn't exist, one will be created
        if (singletonInstance == null)
        {
            singletonInstance = new GameManagerSingleton();
        }

        //returns our instance
        return (GameManagerSingleton)singletonInstance;
    }

    FileIOCommand.LoadCommand loadCommand;
}