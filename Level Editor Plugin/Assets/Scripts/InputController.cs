﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectSelectionCommand;
using FileIOCommand;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputController : MonoBehaviour
{
    public List<GameObject> levelObjects; 
    //public string fileName = "Temp.json";
    public InputField inputFileName;


    private void Update() 
    {
        //Update the values from the public variables above, and place them into the commands
        SpawnCommand.levelObjects = levelObjects;
        string fileName = inputFileName.text + ".json";
        FileCommand.fileName = fileName.Replace(" ", string.Empty);

        //Check if the button to launch specific commands have been called
        //Spawn
        //TODO: Change this when you change the objects being spawned obviously
        //As well as the ENUM names found in the BaseCommand namespace in the CommandPattern.cs file
        //Remember to remove this comment when done
        #region Spawning1
        if (Input.GetKey(KeyCode.Alpha1) && Input.GetMouseButtonDown(0))
        {
            //If holding 1 and you left click
            command_NumClick.execute(BaseCommand.ObjectType.GREENCUBE);
        }
        else if (Input.GetKey(KeyCode.Alpha2) && Input.GetMouseButtonDown(0))
        {
            //If holding 2 and you left click
            command_NumClick.execute(BaseCommand.ObjectType.PURPLECUBE);
        }
        else if (Input.GetKey(KeyCode.Alpha3) && Input.GetMouseButtonDown(0))
        {
            //If holding 3 and you left click
            command_NumClick.execute(BaseCommand.ObjectType.REDCUBE);
        }
        else if (Input.GetKey(KeyCode.Alpha4) && Input.GetMouseButtonDown(0))
        {
            //If holding 4 and you left click
            command_NumClick.execute(BaseCommand.ObjectType.YELLOWCUBE);
        }
        else if (Input.GetKey(KeyCode.Alpha5) && Input.GetMouseButtonDown(0))
        {
            //If holding 5 and you left click
            command_NumClick.execute(BaseCommand.ObjectType.BLUECUBE);
        }
        #endregion
        
        #region Undo/Redo
        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                command_NumClick.Undo();
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                command_NumClick.Redo();
            }
        }
        #endregion

        //Selection/Dragging
        #region Selection
        //Put select code here

        //Dragging the thinger around
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                DragCommand.selectedObject = hit.collider.gameObject;
                DragCommand.objectSelected = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
            DragCommand.objectSelected = false;

        if (DragCommand.objectSelected)
        {
            command_HoldDrag.execute();
        }
        #endregion

        //File Saving and Loading
        #region File I/O
        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                command_CtrlS.execute();
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                command_CtrlL.execute();
            }
        }
        #endregion

    }

    #region ButtonCalls
    public void SpawnButton(string objectType)
    {
        command_NumClick.execute((BaseCommand.ObjectType) System.Enum.Parse(typeof(BaseCommand.ObjectType), objectType));
    }

    public void SaveButton()
    {
        command_CtrlS.execute();
    }

    public void LoadButton()
    {
        command_CtrlL.execute();
    }

    public void UndoButton()
    {
        command_NumClick.Undo();
    }

    public void RedoButton()
    {
        command_NumClick.Redo();
    }

    public void ClearButton()
    {
        command_CtrlL.ClearGame();
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("SpiderCar");
    }
    #endregion
    //Different command objects go under here
    //Unity doesn't use pointers so I can't just use Command*
    SpawnCommand command_NumClick = new SpawnCommand();
    DragCommand command_HoldDrag = new DragCommand();
    SelectCommand command_Click = new SelectCommand();
    SaveCommand command_CtrlS = new SaveCommand();
    LoadCommand command_CtrlL = new LoadCommand();

}
