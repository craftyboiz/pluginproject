﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnStart : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        GameObject go = Instantiate(prefab);
        go.transform.position = transform.position;
        Destroy(gameObject);
    }
}
