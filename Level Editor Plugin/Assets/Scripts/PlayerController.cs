﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public WheelCollider fl;
    public WheelCollider fr;
    public WheelCollider bl;
    public WheelCollider br;

    public float startSpeed;
    public float incRate;
    private float tempSpeed;

    private GameObject grappledPoint;

    private float grappleDist;
    
    private LineRenderer lr;
    private Rigidbody rb;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    { 
        


        if(Input.GetMouseButtonDown(0))
        {
            Ray raymond = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(raymond, out hit))
            {
                if (hit.collider.tag == "5")
                {
                    grappledPoint = hit.collider.gameObject;
                }
            }

            
        }

        if(Input.GetMouseButton(0) && grappledPoint != null)
        {
            //float d = Vector3.Distance(transform.position, grappledPoint.transform.position);
            //if (d < grappleDist)
            //    grappleDist = d;
            //
            //if (Input.mouseScrollDelta.y < 0)
            //    grappleDist += Input.mouseScrollDelta.y;
            //
            //transform.position = (transform.position - grappledPoint.transform.position).normalized * d + grappledPoint.transform.position;
            rb.velocity = Vector3.Project(rb.velocity, Vector3.Cross((grappledPoint.transform.position - transform.position).normalized, Vector3.forward));
        }

        if(Input.GetMouseButtonUp(0))
        {
            grappledPoint = null;
        }

        Vector3[] points = new Vector3[2];
        points[0] = transform.position;
        if (grappledPoint != null)
            points[1] = grappledPoint.transform.position;
        else
            points[1] = transform.position;

        lr.SetPositions(points);

        if (Input.GetKey(KeyCode.D))
        {
            tempSpeed += incRate * Time.deltaTime;
            SetTorqueForAll(tempSpeed);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            tempSpeed += incRate * Time.deltaTime;
            SetTorqueForAll(-tempSpeed);
        }


        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        {
            tempSpeed = startSpeed;
            SetTorqueForAll(0);
        }
    }


    void SetTorqueForAll(float torque)
    {
        fl.motorTorque = torque;
        fr.motorTorque = torque;
        bl.motorTorque = torque;
        br.motorTorque = torque;
    }
}
