﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseCommand;

namespace FileIOCommand
{
    #region File I/O
    class FileCommand : Command
    {
        public override void execute()
        {
            //Debug logs that this is a base object
            Debug.Log("Please use child classes SaveCommand or LoadCommand to utilize file commands");
        }

        public static string fileName;
    }
    class SaveCommand : FileCommand
    {
        public override void execute()
        {
            //Saves our current scene to a JSON file
            Save();
        }

        public void Save()
        {
            DLLFunctions.ClearList();
            
            for (int i = 0; i < objectsInScene.Count; i++)
            {
                //Gets game object
                GameObject obj = objectsInScene[i];
                float[] pos = { obj.transform.position.x, obj.transform.position.y, obj.transform.position.z };
                float[] rot = { obj.transform.rotation.eulerAngles.x, obj.transform.rotation.eulerAngles.y, obj.transform.rotation.eulerAngles.z };

                //Add object to scene list
                DLLFunctions.AddObject(pos, rot, int.Parse(obj.tag));
            }

            DLLFunctions.SaveScene();

            DLLFunctions.SaveFile(fileName);
        }
    }

    class LoadCommand : FileCommand
    {
        public override void execute()
        {
            //Loads our current scene from a JSON file
            Load();
        }

        public void Load()
        {
            ClearGame();

            DLLFunctions.LoadFile(fileName);
            DLLFunctions.LoadScene();

            int objectCount = DLLFunctions.GetObjectCount();
            for (int i = 0; i < objectCount; i++)
            {
                Vector3 pos = new Vector3(DLLFunctions.GetObjectPosX(i), DLLFunctions.GetObjectPosY(i), DLLFunctions.GetObjectPosZ(i));

                Vector3 rot = new Vector3(DLLFunctions.GetObjectRotX(i), DLLFunctions.GetObjectRotY(i), DLLFunctions.GetObjectRotZ(i));

                ObjectType type = (ObjectType)DLLFunctions.GetObjectType(i);

                GameObject temp = MonoBehaviour.Instantiate(levelObjects[(int)type], pos, Quaternion.Euler(rot.x, rot.y, rot.z));
                objectsInScene.Add(temp);
            }
        }

        public void ClearGame()
        {
            //TODO: When this is called
            //Make sure selectables are cleared
            for (int i = 0; i < objectsInScene.Count; i++)
            {
                MonoBehaviour.Destroy(objectsInScene[i]);
            }

            objectsInScene.Clear();

            DLLFunctions.ClearList();
        }
    }
    #endregion
}