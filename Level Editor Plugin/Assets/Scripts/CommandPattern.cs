﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace BaseCommand
{
    //Enum to indicate each prefab for Level editor
    public enum ObjectType
    {
        GREENCUBE,
        PURPLECUBE,
        REDCUBE,
        YELLOWCUBE,
        BLUECUBE,
        GRAPPLEPOINT,
        LIGHT,
        PLAYERSPAWN,
        RAMPR,
        RAMPL,
        POINT,
    }

    //Holds info needed to reinstantiate a prefab at same place
    public struct ObjectInfo
    {
        public ObjectInfo(Vector3 _position, Vector3 _rotation, ObjectType _type)
        {
            position = _position;
            rotation = _rotation;
            type = _type;
        }

        public Vector3 position;
        public Vector3 rotation;
        public ObjectType type;

    } 
    
    class DLLFunctions 
    {
        #region DLL IMPORTS
        const string DLL_NAME = "SIMPLEPLUGIN";

        //Saves the objects to the json object
        [DllImport(DLL_NAME)]
        public static extern void SaveScene();
        //Saves the json object to the file
        [DllImport(DLL_NAME)]
        public static extern void SaveFile([MarshalAs(UnmanagedType.LPStr)]string name);
    
        //Loads the json object in from the file
        [DllImport(DLL_NAME)]
        public static extern void LoadFile([MarshalAs(UnmanagedType.LPStr)]string name);
        //Loads in the objects from the json object
        [DllImport(DLL_NAME)]
        public static extern void LoadScene();

        //Add Object to the list
        [DllImport(DLL_NAME)]
        public static extern void AddObject(float[] _position, float[] _rotation, int _type);

        //Gets position components
        [DllImport(DLL_NAME)]
        public static extern float GetObjectPosX(int index);
        [DllImport(DLL_NAME)]
        public static extern float GetObjectPosY(int index);
        [DllImport(DLL_NAME)]
        public static extern float GetObjectPosZ(int index);
    
        //Gets rotation components
        [DllImport(DLL_NAME)]
        public static extern float GetObjectRotX(int index);
        [DllImport(DLL_NAME)]
        public static extern float GetObjectRotY(int index);
        [DllImport(DLL_NAME)]
        public static extern float GetObjectRotZ(int index);

        //Gets object type
        [DllImport(DLL_NAME)]
        public static extern int GetObjectType(int index);
    
        //Gets the object count
        [DllImport(DLL_NAME)]
        public static extern int GetObjectCount();

        //Clears out the list to prep for new save version
        [DllImport(DLL_NAME)]
        public static extern void ClearList();
        #endregion
    }

    class Command
    {
        public virtual void execute() 
        { 
            //Default command debug log message
            Debug.Log("There is no command set to this input."); 
        }

        public virtual void execute(ObjectType target)
        {

        }
        
        //all commands require access to the list of objects in scene
        public static List<GameObject> objectsInScene = new List<GameObject>();

        //Prefabs to spawn
        public static List<GameObject> levelObjects;  
    }
}


